<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Product>
 *
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function save(Product $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Product $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findAllAndCategories(): array
    {
        return $this->createQueryBuilder('p')
            ->leftJoin('p.category_id', 'c')
            ->addSelect('c')
            ->getQuery()
            ->getResult();
    }

    public function getTotalProductsCategory()
    {
        $qb = $this->createQueryBuilder('p')
            ->select('c.id as id_categ, COUNT(p.id) as total_products')
            ->join('p.category_id', 'c')
            ->groupBy('c.id')
            ->getQuery();

        $res = ['list', 'data'];

        foreach ($qb->getResult() as $key => $data) {
            $res['list'][] = $data['id_categ'];
            $res['data'][$data['id_categ']] = $data['total_products'];
        }

        return $res;
    }

    public function findAllByCateg(int $id): ?array
    {
        $qb = $this->createQueryBuilder('p')
            ->where('p.category_id = :categoryId')
            ->setParameter('categoryId', $id);

        return $qb->getQuery()->getResult();
    }

    public function findAllByName(string $name)
    {
        $qb = $this->createQueryBuilder('p')
            ->where('p.name LIKE :search')
            ->setParameter('search', '%' . $name . '%');

        return $qb->getQuery()->getResult();
    }

    //    /**
    //     * @return Product[] Returns an array of Product objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('p')
    //            ->andWhere('p.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('p.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Product
    //    {
    //        return $this->createQueryBuilder('p')
    //            ->andWhere('p.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
