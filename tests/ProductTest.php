<?php

namespace App\Tests;

use DateTime;
use App\Entity\Product;
use App\Entity\Category;
use PHPUnit\Framework\TestCase;

class ProductTest extends TestCase
{
    public function testIsTrue(): void
    {
        $product = new Product();
        $category = new Category();

        $product->setName('Product 1')
            ->setCreatedAt(new DateTime('2023-04-04 12:12:12'))
            ->setModifiedAt(new DateTime('2023-04-04 12:12:12'))
            ->setCategoryId($category)
            ->setDescription('Ceci est la description')
            ->setImages('/images/products/prod-1.png')
            ->setPrice(10.2);

        $this->assertTrue($product->getName() === 'Product 1');
        $this->assertTrue($product->getCreatedAt()->format('Y-m-d H:i:s') === '2023-04-04 12:12:12');
        $this->assertTrue($product->getModifiedAt()->format('Y-m-d H:i:s') === '2023-04-04 12:12:12');
        $this->assertTrue($product->getCategoryId() === $category);
        $this->assertTrue($product->getDescription() === 'Ceci est la description');
        $this->assertTrue($product->getImages() === '/images/products/prod-1.png');
        $this->assertTrue($product->getPrice() === '10.2');
    }

    public function testIsFalse(): void
    {
        $product = new Product();
        $category = new Category();

        $product->setName('Product 2')
            ->setCreatedAt(new DateTime())
            ->setModifiedAt(new DateTime())
            ->setCategoryId($category)
            ->setDescription('Ceci est une description')
            ->setImages('/images/products/prod-2.png')
            ->setPrice(10.3);

        $this->assertFalse($product->getName() === 'Product 1');
        $this->assertFalse($product->getCreatedAt()->format('Y-m-d H:i:s') === '2023-04-04 12:12:12');
        $this->assertFalse($product->getModifiedAt()->format('Y-m-d H:i:s') === '2023-04-04 12:12:12');
        $this->assertFalse($product->getCategoryId() === new Category());
        $this->assertFalse($product->getDescription() === 'Ceci est la description');
        $this->assertFalse($product->getImages() === '/images/products/prod-1.png');
        $this->assertFalse($product->getPrice() === 10.2);
    }

    public function testIsEmpty(): void
    {
        $product = new Product();

        $this->assertEmpty($product->getName());
        $this->assertEmpty($product->getCreatedAt());
        $this->assertEmpty($product->getModifiedAt());
        $this->assertEmpty($product->getCategoryId());
        $this->assertEmpty($product->getDescription());
        $this->assertEmpty($product->getImages());
        $this->assertEmpty($product->getPrice());
    }
}
