<?php

namespace App\Tests;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testIsTrue(): void
    {
        $user = new User();

        $user->setEmail('true@test.com')
            ->setName('Paul')
            ->setPassword('pass');

        $this->assertTrue($user->getEmail() === 'true@test.com');
        $this->assertTrue($user->getName() === 'Paul');
        $this->assertTrue($user->getPassword() === 'pass');
    }

    public function testIsFalse(): void
    {
        $user = new User();

        $user->setEmail('false@test.com')
            ->setName('Yvon')
            ->setPassword('password');

        $this->assertFalse($user->getEmail() === 'true@test.com');
        $this->assertFalse($user->getName() === 'Paul');
        $this->assertFalse($user->getPassword() === 'pass');
    }

    public function testIsEmpty(): void
    {
        $user = new User();

        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getName());
        $this->assertEmpty($user->getPassword());
    }
}
