<?php

namespace App\Tests;

use DateTime;
use App\Entity\Category;
use PHPUnit\Framework\TestCase;

class CategoryTest extends TestCase
{
    public function testIsTrue(): void
    {
        $category = new Category();

        $category->setName('Categorie 1')
            ->setDescription('Ceci est une description')
            ->setCreatedAt(new DateTime('2023-04-04 12:12:12'))
            ->setModifiedAt(new DateTime('2023-04-04 12:12:12'));

        $this->assertTrue($category->getName() === 'Categorie 1');
        $this->assertTrue($category->getDescription() === 'Ceci est une description');
        $this->assertTrue($category->getCreatedAt()->format('Y-m-d H:i:s') === '2023-04-04 12:12:12');
        $this->assertTrue($category->getModifiedAt()->format('Y-m-d H:i:s') === '2023-04-04 12:12:12');
    }

    public function testIsFalse(): void
    {
        $category = new Category();

        $category->setName('Categorie 2')
            ->setDescription('Ceci est une description 2')
            ->setCreatedAt(new DateTime())
            ->setModifiedAt(new DateTime());

        $this->assertFalse($category->getName() === 'Categorie 1');
        $this->assertFalse($category->getDescription() === 'Ceci est une description');
        $this->assertFalse($category->getCreatedAt()->format('Y-m-d H:i:s') === '2023-04-04 12:12:12');
        $this->assertFalse($category->getModifiedAt()->format('Y-m-d H:i:s') === '2023-04-04 12:12:12');
    }

    public function testIsEmpty(): void
    {
        $category = new Category();

        $this->assertEmpty($category->getName());
        $this->assertEmpty($category->getDescription());
        $this->assertEmpty($category->getCreatedAt());
        $this->assertEmpty($category->getModifiedAt());
    }
}
